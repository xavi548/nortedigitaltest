<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = "clients";

    protected $fillable = 
    [
        "name",
        "last_name"
    ];

    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice');
    }

}
