<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = "invoices";

    public function sales()
    {
        return $this->hasMany('App\Models\Sale');
    }

    public function client()
    {
        return $this->belongsTo("App\Models\Client");
    }
}
