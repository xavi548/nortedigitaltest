<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Product;
use App\Models\Sale;
use App\Models\Invoice;

class InvoiceController extends Controller
{
    public function index()
    {
        $invoice = Invoice::find(1);
        $total = 0;
        $sales = $invoice->sales->map(function($current) use (&$total)
        {
            $product = Product::findOrFail($current->product_id);
            $subTotal = $product->price * $current->quantity;
            $total += $subTotal;
            return [
                "quantity"  => $current->quantity,
                "product"   => $product->product,
                "price"     => $product->price,
                "sub_total" => $subTotal
            ];
        });
        $response =[
            'id'        => $invoice->id,
            'client'    => $invoice->client->only(['name', 'last_name']),
            'detail'    => $sales,
            'total'     => $total
        ];




        return ($response);
        
    }
}
