<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Sales extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // USERS
        DB::table('clients')->insert([
            'name'      => 'Xavier',
            'last_name' => 'Moreno',
        ]);
        DB::table('clients')->insert([
            'name'      => 'Alexis',
            'last_name' => 'Guerrero',
        ]);

        // PRODUCTS
        DB::table('products')->insert([
            'product' => 'Bebida Cocacola 1LT',
            'price'   => 500,
        ]);
        DB::table('products')->insert([
            'product' => 'Bebida Sprite 1LT',
            'price'   => 500,
        ]);
        // INVOICES
        DB::table('invoices')->insert([
            'client_id' => 1,
        ]);

        // SALES
        DB::table('sales')->insert([
            'invoice_id' => 1,
            'product_id' => 1,
            'quantity'   => 1
        ]);
        DB::table('sales')->insert([
            'invoice_id' => 1,
            'product_id' => 2,
            'quantity'   => 2
        ]);

        
    }
}
