# Dependecias

laravel "5.7"
PHP "7.2"
Mysql " 5.7.25"

# Instrucciones

clone en un directorio deseado y a continuacioón, ejecute los siguientes comandos en la raiz del proyecto:

`$ composer install`

Seguido del comando anterior, haga una copia del archivo .env.example y noombrelo .env, o ejecute el siguiente comando en linux

`$ cp .env.example .env`

Seguidamente escriba las credenciales del gestor de base de datos local mysql, en las siguientes lineas del archivo .env, habiendo creado anteriormente una base de datos, ya sea en la terminal o en un gestor, recomiendo usar mysql workbench.

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=databasename
DB_USERNAME=username
DB_PASSWORD=password

luego ejecute los siguientes comandos en orden

`$ php artisan key:generate`

`$ php artisan migrate`

`$ php artisan db:seed`

`$ php artisan serve`


Dirijase a la ruta http://localhost:8000/api/